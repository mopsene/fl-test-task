package com.mopsene.fl.test.listener;

import com.mopsene.fl.test.event.PageSyncEvent;
import com.mopsene.fl.test.model.entity.Genre;
import com.mopsene.fl.test.model.entity.Movie;
import com.mopsene.fl.test.service.CalculatingService;
import mockit.Mock;
import mockit.MockUp;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertTrue;

class PageSynсListenerTest {
    private CalculatingService calculatingService;
    private PageSynсListener pageSynсListener;

    @BeforeEach
    void setUp() {
        calculatingService = new CalculatingService();
        pageSynсListener = new PageSynсListener(calculatingService);
    }

    @Test
    void onApplicationEvent_ValidData_VerifyCorrectCalculatingCall() {
        Movie movie1 = new Movie() {{
            setId(1);
            setGenres(Arrays.asList(
                    new Genre() {{
                        setId(1);
                    }},
                    new Genre() {{
                        setId(3);
                    }}
            ));
        }};

        Movie movie2 = new Movie() {{
            setId(3);
            setGenres(Arrays.asList(
                    new Genre() {{
                        setId(2);
                    }},
                    new Genre() {{
                        setId(3);
                    }}
            ));
        }};

        new MockUp<CalculatingService>() {
            @Mock
            public void addPageToCalculatingQueue(Map<Integer, List<Movie>> movies) {
                assertAll(
                        () -> assertTrue(movies.get(1).contains(movie1)),
                        () -> assertTrue(movies.get(3).contains(movie1)),
                        () -> assertTrue(movies.get(2).contains(movie2)),
                        () -> assertTrue(movies.get(3).contains(movie2))
                );
            }
        };

        PageSyncEvent pageSyncEvent = new PageSyncEvent(this, Arrays.asList(movie1, movie2));

        pageSynсListener.onApplicationEvent(pageSyncEvent);
    }
}