package com.mopsene.fl.test.service;

import com.mopsene.fl.test.event.PageSyncEvent;
import com.mopsene.fl.test.model.entity.Genre;
import com.mopsene.fl.test.model.entity.SyncJob;
import com.mopsene.fl.test.service.dao.MovieService;
import com.mopsene.fl.test.service.dao.SyncJobService;
import mockit.Mock;
import mockit.MockUp;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationEventPublisher;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;

import static org.mockito.Mockito.*;

class InitServiceTest {

    private SyncJobService syncJobService;
    private SyncDataService syncDataService;
    private DataFetchService dataFetchService;
    private CalculatingService calculatingService;
    private MovieService movieService;
    private ApplicationEventPublisher applicationEventPublisher;

    private InitService initService;

    @BeforeEach
    void setUp() {
        syncJobService = mock(SyncJobService.class);
        syncDataService = mock(SyncDataService.class);
        dataFetchService = mock(DataFetchService.class);
        movieService = mock(MovieService.class);
        calculatingService = mock(CalculatingService.class);
        applicationEventPublisher = mock(ApplicationEventPublisher.class);
        new MockUp<CompletableFuture>() {
            @Mock
            public CompletableFuture<Void> runAsync(Runnable runnable, Executor executor) {
                return null;
            }
        };
        initService = new InitService(syncJobService, syncDataService, dataFetchService, calculatingService, movieService, applicationEventPublisher);
    }

    @Test
    void test_OnApplicationEvent_ValidData_VerifySyncStarted() {
        Map<Integer, Genre> allGenres = new HashMap<>();
        SyncJob syncJob = new SyncJob(0, 0, null);
        when(syncDataService.syncGenres()).thenReturn(allGenres);
        when(syncJobService.getOrCreateSyncJob()).thenReturn(syncJob);
        when(dataFetchService.getMaxPageCount()).thenReturn(20);

        initService.onApplicationEvent(null);

        verify(applicationEventPublisher, times(1)).publishEvent(any(PageSyncEvent.class));
    }
}