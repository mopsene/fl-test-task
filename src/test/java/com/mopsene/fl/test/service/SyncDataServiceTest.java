package com.mopsene.fl.test.service;

import com.mopsene.fl.test.model.dto.GenreDTO;
import com.mopsene.fl.test.model.dto.GenresDTO;
import com.mopsene.fl.test.model.dto.MovieDTO;
import com.mopsene.fl.test.model.dto.MoviesPageDTO;
import com.mopsene.fl.test.model.entity.Genre;
import com.mopsene.fl.test.model.entity.Movie;
import com.mopsene.fl.test.service.dao.GenreService;
import com.mopsene.fl.test.service.dao.MovieService;
import com.mopsene.fl.test.service.dao.SyncJobService;
import mockit.Mock;
import mockit.MockUp;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationEventPublisher;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Matchers.anyList;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class SyncDataServiceTest {
    private GenreService genreService;
    private MovieService movieService;
    private DataFetchService dataFetchService;
    private SyncJobService syncJobService;
    private ApplicationEventPublisher applicationEventPublisher;

    private SyncDataService syncDataService;

    @BeforeEach
    void setUp() {
        genreService = mock(GenreService.class);
        movieService = mock(MovieService.class);
        dataFetchService = mock(DataFetchService.class);
        syncJobService = mock(SyncJobService.class);
        applicationEventPublisher = mock(ApplicationEventPublisher.class);

        syncDataService = new SyncDataService(genreService, movieService, dataFetchService, syncJobService, applicationEventPublisher);
    }

    @Test
    void test_SyncGenres_ValidData_verifyCorrectSaveAll() {
        List<GenreDTO> genreDTOS = Arrays.asList(new GenreDTO() {{
                                                     setId(1);
                                                 }},
                new GenreDTO() {{
                    setId(4);
                }},
                new GenreDTO() {{
                    setId(18);
                }}
        );

        Genre expectedGenre1 = new Genre() {{
            setId(1);
        }};

        Genre expectedGenre2 = new Genre() {{
            setId(4);
        }};

        Genre expectedGenre3 = new Genre() {{
            setId(18);
        }};
        GenresDTO genresDTO = new GenresDTO() {{
            setGenres(genreDTOS);
        }};

        when(dataFetchService.fetchGenres()).thenReturn(genresDTO);
        when(genreService.saveAll(anyList())).thenReturn(Arrays.asList(expectedGenre1, expectedGenre2, expectedGenre3));


        Map<Integer, Genre> genreMap = syncDataService.syncGenres();

        assertAll(
                () -> assertEquals(expectedGenre1, genreMap.get(expectedGenre1.getId())),
                () -> assertEquals(expectedGenre2, genreMap.get(expectedGenre2.getId())),
                () -> assertEquals(expectedGenre3, genreMap.get(expectedGenre3.getId()))
        );
    }

    @Test
    void test_SyncMovies_ValidResponse_VerifyCorrectSave() {
        Genre genre1 = new Genre(1, "First");
        Genre genre2 = new Genre(2, "Second");
        Map<Integer, Genre> genreMap = new HashMap() {{
            put(1, genre1);
            put(2, genre2);
        }};

        MovieDTO movieDTO1 = new MovieDTO() {{
            setId(12);
            setGenreIds(Arrays.asList(1, 2));
        }};

        MovieDTO movieDTO2 = new MovieDTO() {{
            setId(15);
            setGenreIds(Collections.singletonList(1));
        }};

        when(dataFetchService.fetchMoviesPage(1)).thenReturn(new MoviesPageDTO() {{
            setResults(Arrays.asList(movieDTO1, movieDTO2));
        }});


        new MockUp<SyncDataService>() {
            @Mock
            public List<Movie> savePage(List<Movie> movies, int page) {
                Movie movie1 = movies.stream().filter(m -> m.getId() == 12).findFirst().get();
                Movie movie2 = movies.stream().filter(m -> m.getId() == 15).findFirst().get();
                assertAll(
                        () -> assertTrue(movie1.getGenres().contains(genre1)),
                        () -> assertTrue(movie1.getGenres().contains(genre2)),
                        () -> assertTrue(movie2.getGenres().contains(genre1))
                );
                return null;
            }
        };

        syncDataService.syncMovies(genreMap, 1, 1);


    }

}