package com.mopsene.fl.test.service;

import com.mopsene.fl.test.model.dto.MoviesPageDTO;
import lombok.SneakyThrows;
import mockit.MockUp;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.lang.reflect.Field;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class DataFetchServiceTest {
    private DataFetchService dataFetchService;
    private RestTemplate restTemplate;

    @BeforeEach
    @SneakyThrows
    void init() {
        restTemplate = mock(RestTemplate.class);
        new MockUp<RestTemplateBuilder>() {
            @mockit.Mock
            public RestTemplate build() {
                return restTemplate;
            }

        };
        dataFetchService = new DataFetchService();
        Field movieIntegrationUrl = DataFetchService.class.getDeclaredField("movieIntegrationUrl");
        Field movieIntegrationKey = DataFetchService.class.getDeclaredField("movieIntegrationKey");
        movieIntegrationKey.setAccessible(true);
        movieIntegrationUrl.setAccessible(true);
        movieIntegrationUrl.set(dataFetchService, "http://mocked-url");
        movieIntegrationKey.set(dataFetchService, "very-secret-key");
    }

    @Test
    void test_getMaxPageCount_ValidResponse_AssertPageCount() {
        MoviesPageDTO moviesPageDTO = new MoviesPageDTO() {{
            setTotal_pages(14);
        }};
        when(restTemplate.getForEntity(eq("http://mocked-url/3/discover/movie?api_key=very-secret-key&page=1"),
                eq(MoviesPageDTO.class)))
                .thenReturn(new ResponseEntity<>(moviesPageDTO, HttpStatus.OK));
        dataFetchService.init();
        assertEquals(14, dataFetchService.getMaxPageCount());
    }

    @Test
    void test_getMaxPageCount_ValidNull_AssertPageCountEquals0() {
        MoviesPageDTO body = null;
        when(restTemplate.getForEntity(eq("http://mocked-url/3/discover/movie?api_key=very-secret-key&page=1"),
                eq(MoviesPageDTO.class)))
                .thenReturn(new ResponseEntity<>(body, HttpStatus.PAYMENT_REQUIRED));
        dataFetchService.init();
        assertEquals(0, dataFetchService.getMaxPageCount());
    }
}
