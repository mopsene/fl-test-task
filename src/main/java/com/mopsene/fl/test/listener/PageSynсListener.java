package com.mopsene.fl.test.listener;

import com.mopsene.fl.test.event.PageSyncEvent;
import com.mopsene.fl.test.model.entity.Movie;
import com.mopsene.fl.test.service.CalculatingService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;

@Component
@RequiredArgsConstructor
@Slf4j
public class PageSynсListener implements ApplicationListener<PageSyncEvent> {
    private final CalculatingService calculatingService;

    @Override
    public void onApplicationEvent(PageSyncEvent event) {
        try {
            Map<Integer, List<Movie>> moviesMap = event.getMovies().stream()
                    .flatMap(m -> m.getGenres().stream()
                            .map(g -> Pair.of(g.getId(), m)))
                    .collect(groupingBy(Pair::getFirst, Collectors.mapping(Pair::getSecond, toList())));
            calculatingService.addPageToCalculatingQueue(moviesMap);
        } catch (Exception e) {
            log.error("Cannot add page to calculating!", e);
        }
    }
}
