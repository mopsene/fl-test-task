package com.mopsene.fl.test.controller;

import com.mopsene.fl.test.model.dto.AverageVoteDTO;
import com.mopsene.fl.test.service.CalculatingService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/movies")
@RequiredArgsConstructor
public class MoviesController {
    private final CalculatingService calculatingService;

    @GetMapping("/get/{genreId}")
    public AverageVoteDTO getAverageVoteByGenreId(@PathVariable("genreId") Integer genreId) {
        return calculatingService.getAverageVote(genreId);
    }

    @GetMapping("/start/{genreId}")
    public void startCalculatingByGenreId(@PathVariable("genreId") Integer genreId) {
        calculatingService.continueCalculating(genreId);
    }

    @GetMapping("/pause/{genreId}")
    public void pauseCalculatingByGenreId(@PathVariable("genreId") Integer genreId) {
        calculatingService.pauseCalculating(genreId);
    }
}
