package com.mopsene.fl.test.model.dto;


import com.fasterxml.jackson.annotation.JsonAutoDetect;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@JsonAutoDetect
public class GenresDTO {
    List<GenreDTO> genres;
}
