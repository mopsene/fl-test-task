package com.mopsene.fl.test.model.dto;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@JsonAutoDetect
@Data
@NoArgsConstructor
public class MovieDTO {
    private int id;
    @JsonProperty("vote_average")
    private double voteAverage;
    private String title;
    @JsonProperty("genre_ids")
    private List<Integer> genreIds;
}
