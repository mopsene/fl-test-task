package com.mopsene.fl.test.model.dto;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@JsonAutoDetect
public class MoviesPageDTO {
    private int page;
    private int total_results;
    private int total_pages;
    private List<MovieDTO> results;
}
