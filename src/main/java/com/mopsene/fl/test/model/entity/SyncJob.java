package com.mopsene.fl.test.model.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

/**
 * Entity for restore App. Sync will be continued from page in this class
 */
@Entity
@Getter
@Table(name = "sync_job")
@NoArgsConstructor
@AllArgsConstructor
public class SyncJob {
    @Id
    @Column(name = "id")
    private int id;

    @Column(name = "last_page")
    private int lastPage;

    @Column(name = "last_update")
    @Temporal(TemporalType.DATE)
    private Date lastUpdate;
}
