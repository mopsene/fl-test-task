package com.mopsene.fl.test.model.dto;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mopsene.fl.test.model.entity.Movie;
import lombok.Data;

import java.util.LinkedList;

@Data
@JsonAutoDetect
public class AverageVoteDTO {
    private GenreDTO genre;
    private double vote;
    private int calculatedMovies;
    @JsonIgnore
    private final LinkedList<Movie> queueMovies = new LinkedList<>();

    public AverageVoteDTO(GenreDTO genre) {
        this.genre = genre;
    }
}
