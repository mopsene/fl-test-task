package com.mopsene.fl.test.model.dto;

import lombok.Data;

@Data
public class CalculatingMovieTask {
    private int id;
    private int vote;
    private boolean calculated;
}
