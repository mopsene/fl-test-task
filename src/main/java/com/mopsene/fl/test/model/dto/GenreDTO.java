package com.mopsene.fl.test.model.dto;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@JsonAutoDetect
@AllArgsConstructor
public class GenreDTO {
    private int id;
    private String name;
}