package com.mopsene.fl.test.event;

import com.mopsene.fl.test.model.entity.Movie;
import lombok.Data;
import org.springframework.context.ApplicationEvent;

import java.util.List;

@Data
public class PageSyncEvent extends ApplicationEvent {
    private final List<Movie> movies;

    public PageSyncEvent(Object source, List<Movie> movies) {
        super(source);
        this.movies = movies;
    }
}
