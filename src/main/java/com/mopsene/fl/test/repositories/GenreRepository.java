package com.mopsene.fl.test.repositories;

import com.mopsene.fl.test.model.entity.Genre;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GenreRepository extends JpaRepository<Genre, Integer> {

}
