package com.mopsene.fl.test.repositories;

import com.mopsene.fl.test.model.entity.SyncJob;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;

public interface SyncJobRepository extends JpaRepository<SyncJob, Integer> {
    @Modifying
    @Query(value = "UPDATE sync_job sj SET last_page = ?2, last_update = ?3 WHERE sj.id = ?1",
            nativeQuery = true)
    void updateSyncJob(int id, int lastPage, Date lastUpdate);
}
