package com.mopsene.fl.test.repositories;

import com.mopsene.fl.test.model.entity.Movie;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MovieRepository extends JpaRepository<Movie, Integer> {
}
