package com.mopsene.fl.test.service;

import com.mopsene.fl.test.event.PageSyncEvent;
import com.mopsene.fl.test.model.entity.Genre;
import com.mopsene.fl.test.model.entity.Movie;
import com.mopsene.fl.test.service.dao.GenreService;
import com.mopsene.fl.test.service.dao.MovieService;
import com.mopsene.fl.test.service.dao.SyncJobService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class SyncDataService {
    private final GenreService genreService;
    private final MovieService movieService;
    private final DataFetchService dataFetchService;
    private final SyncJobService syncJobService;
    private final ApplicationEventPublisher applicationEventPublisher;

    @Transactional
    public Map<Integer, Genre> syncGenres() {
        List<Genre> genres = dataFetchService.fetchGenres().getGenres()
                .stream()
                .map(g -> new Genre(g.getId(), g.getName()))
                .collect(Collectors.toList());
        return genreService.saveAll(genres).stream().collect(Collectors.toMap(Genre::getId, g -> g));
    }

    public void syncMovies(Map<Integer, Genre> genreMap, int pageFrom, int pageTo) {
        int currentPage = pageFrom;
        while (currentPage <= pageTo) {
            try {
                List<Movie> movies = dataFetchService.fetchMoviesPage(currentPage).getResults()
                        .stream()
                        .map(m -> new Movie(m.getId(), m.getVoteAverage(), m.getTitle(), transformGenres(genreMap, m.getGenreIds())))
                        .collect(Collectors.toList());
                movies = savePage(movies, currentPage);
                applicationEventPublisher.publishEvent(new PageSyncEvent(this, movies));
            } catch (Exception e) {
                log.error("Cannot sync movie page = {}", currentPage, e);
            }
            currentPage++;
        }
    }

    private List<Genre> transformGenres(Map<Integer, Genre> genreMap, List<Integer> ids) {
        return ids.stream().map(genreMap::get).collect(Collectors.toList());
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public List<Movie> savePage(List<Movie> movies, int page) {
        List<Movie> savedMovies = movieService.saveAll(movies);
        syncJobService.updateSyncJob(page);
        return savedMovies;
    }
}
