package com.mopsene.fl.test.service.dao;

import com.mopsene.fl.test.model.entity.SyncJob;
import com.mopsene.fl.test.repositories.SyncJobRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
@RequiredArgsConstructor
public class SyncJobService {
    private final SyncJobRepository syncJobRepository;

    @Transactional
    public SyncJob getOrCreateSyncJob() {
        SyncJob syncJob;
        synchronized (this) {
            List<SyncJob> all = syncJobRepository.findAll();
            if (all.size() > 1) {
                throw new RuntimeException("There are more than one Sync Job in Database!");
            }
            syncJob = all.stream().findFirst().orElseGet(this::initNewSyncJob);
        }
        return syncJob;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void updateSyncJob(int page) {
        syncJobRepository.updateSyncJob(0, page, new Date());
    }

    private SyncJob initNewSyncJob() {
        return syncJobRepository.save(new SyncJob(0, 0, new Date()));
    }
}
