package com.mopsene.fl.test.service.dao;

import com.mopsene.fl.test.model.entity.Genre;
import com.mopsene.fl.test.repositories.GenreRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class GenreService {
    private final GenreRepository genreRepository;

    public Genre save(Genre genre) {
        return genreRepository.save(genre);
    }

    public List<Genre> saveAll(List<Genre> genres) {
        return genreRepository.saveAll(genres);
    }
}
