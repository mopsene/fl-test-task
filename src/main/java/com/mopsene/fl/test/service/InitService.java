package com.mopsene.fl.test.service;

import com.mopsene.fl.test.event.PageSyncEvent;
import com.mopsene.fl.test.model.entity.Genre;
import com.mopsene.fl.test.model.entity.SyncJob;
import com.mopsene.fl.test.service.dao.MovieService;
import com.mopsene.fl.test.service.dao.SyncJobService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Component
@Slf4j
@RequiredArgsConstructor
@Lazy(value = false)
public class InitService implements ApplicationListener<ContextRefreshedEvent> {
    private final SyncJobService syncJobService;
    private final SyncDataService syncDataService;
    private final DataFetchService dataFetchService;
    private final CalculatingService calculatingService;
    private final MovieService movieService;
    private final ApplicationEventPublisher applicationEventPublisher;
    private final ExecutorService executorService = Executors.newSingleThreadExecutor();

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        log.info("Init the movie application");
        Map<Integer, Genre> allGenres = syncDataService.syncGenres();
        calculatingService.initCalculatingTasks(allGenres.values());
        applicationEventPublisher.publishEvent(new PageSyncEvent(this, movieService.findAll()));
        SyncJob syncJob = syncJobService.getOrCreateSyncJob();
        int maxPageCount = dataFetchService.getMaxPageCount();
        int startPagesForSync = syncJob.getLastPage() + 1;
        if (startPagesForSync >= maxPageCount) {
            return;
        }
        log.info("Sync from {} to {} pages has been started", startPagesForSync, maxPageCount);
        CompletableFuture.runAsync(() -> syncDataService.syncMovies(allGenres, startPagesForSync, maxPageCount),
                executorService);
    }
}
