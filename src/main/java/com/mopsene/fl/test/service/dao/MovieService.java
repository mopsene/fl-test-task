package com.mopsene.fl.test.service.dao;

import com.mopsene.fl.test.model.entity.Movie;
import com.mopsene.fl.test.repositories.MovieRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
public class MovieService {
    private final MovieRepository movieRepository;

    @Transactional(propagation = Propagation.REQUIRED)
    public List<Movie> saveAll(List<Movie> movies) {
        return movieRepository.saveAll(movies);
    }

    @Transactional
    public List<Movie> findAll() {
        return movieRepository.findAll();
    }
}
