package com.mopsene.fl.test.service;

import com.mopsene.fl.test.model.dto.GenresDTO;
import com.mopsene.fl.test.model.dto.MoviesPageDTO;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.util.Optional;

@Service
public class DataFetchService {
    private final String GENRES_BASE_METHOD = "/3/genre/movie/list?api_key=%s";
    private final String MOVIE_PAGE_BASE_METHOD = "/3/discover/movie?api_key=%s";
    private final RestTemplate restTemplate = new RestTemplateBuilder().build();

    @Value("${movie.integration.url}")
    private String movieIntegrationUrl;

    @Value("${movie.integration.key}")
    private String movieIntegrationKey;

    private String genreUrl;
    private String moviesFullUrl;

    @PostConstruct
    public void init() {
        genreUrl = String.format(movieIntegrationUrl + GENRES_BASE_METHOD, movieIntegrationKey);
        moviesFullUrl = String.format(movieIntegrationUrl + MOVIE_PAGE_BASE_METHOD, movieIntegrationKey) + "&page=%s";
    }

    public GenresDTO fetchGenres() {
        return restTemplate.getForEntity(genreUrl,
                GenresDTO.class
        ).getBody();
    }


    public MoviesPageDTO fetchMoviesPage(int page) {
        return restTemplate.getForEntity(String.format(moviesFullUrl, page),
                MoviesPageDTO.class
        ).getBody();
    }

    public int getMaxPageCount() {
        ResponseEntity<MoviesPageDTO> firstPage = restTemplate.getForEntity(String.format(moviesFullUrl, 1),
                MoviesPageDTO.class
        );
        return Optional.ofNullable(firstPage.getBody()).orElseGet(MoviesPageDTO::new).getTotal_pages();
    }
}
