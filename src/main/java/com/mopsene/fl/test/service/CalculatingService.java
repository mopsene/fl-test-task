package com.mopsene.fl.test.service;

import com.mopsene.fl.test.model.dto.AverageVoteDTO;
import com.mopsene.fl.test.model.dto.GenreDTO;
import com.mopsene.fl.test.model.entity.Genre;
import com.mopsene.fl.test.model.entity.Movie;
import lombok.AllArgsConstructor;
import lombok.Setter;
import lombok.SneakyThrows;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Service
public class CalculatingService {
    public static final String GENRE_NOT_FOUND_MSG = "Genre with id=%s has not found!";
    private final Map<Integer, AverageVoteDTO> votesMap = new ConcurrentHashMap<>();
    private final Map<Integer, AverageCalculatingTask> tasksMap = new HashMap<>();
    private final ExecutorService executorService = Executors.newFixedThreadPool(20);

    public AverageVoteDTO getAverageVote(int genreId) {
        return Optional.ofNullable(votesMap.get(genreId))
                .orElseThrow(() -> new RuntimeException(String.format(GENRE_NOT_FOUND_MSG, genreId)));
    }

    public void continueCalculating(Integer genreId) {
        Optional.ofNullable(tasksMap.get(genreId))
                .ifPresent(t -> t.calculating = true);
    }


    public void pauseCalculating(Integer genreId) {
        Optional.ofNullable(tasksMap.get(genreId))
                .ifPresent(t -> t.calculating = false);
    }

    public void initCalculatingTasks(Collection<Genre> genres) {
        genres.forEach(g -> {
            AverageVoteDTO averageVoteDTO = new AverageVoteDTO(new GenreDTO(g.getId(), g.getName()));
            votesMap.put(g.getId(), averageVoteDTO);
            tasksMap.put(g.getId(), new AverageCalculatingTask(averageVoteDTO, false));
        });
        tasksMap.values().forEach(executorService::submit);
    }

    public void addPageToCalculatingQueue(Map<Integer, List<Movie>> movies) {
        movies.forEach((key, value) -> {
            AverageVoteDTO averageVoteDTO = votesMap.get(key);
            averageVoteDTO.getQueueMovies().addAll(value);
        });
    }

    @AllArgsConstructor
    @Setter
    class AverageCalculatingTask implements Runnable {
        private final AverageVoteDTO averageVoteDTO;
        private boolean calculating;

        @Override
        @SneakyThrows
        public void run() {
            while (true) {
                if (calculating) {
                    Optional.ofNullable(averageVoteDTO.getQueueMovies().poll()).ifPresent(m -> {
                        averageVoteDTO.setVote((averageVoteDTO.getVote() * averageVoteDTO.getCalculatedMovies()
                                + m.getVoteAverage()) / (averageVoteDTO.getCalculatedMovies() + 1));
                        averageVoteDTO.setCalculatedMovies(averageVoteDTO.getCalculatedMovies() + 1);
                    });
                }
                Thread.sleep(100);
            }
        }
    }
}
