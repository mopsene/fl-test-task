#!/bin/bash
pg_ctl -w stop
rm -rf /var/run/postgresql/*
rm -rf $PGDATA
initdb --username=postgres
echo "host all all all trust" >> $PGDATA/pg_hba.conf
pg_ctl -w start
psql -c "ALTER USER postgres WITH PASSWORD 'postgres';"
psql -c "create USER app WITH PASSWORD 'app';"
psql -c "create database app with owner app;"
pg_ctl -w stop
postgres